<?php
// Canvas Grade Report Generator
// v1.1.1
// @mattlibera
?>

<?php
// uncomment to force HTTPS without relying on .htaccess
// if($_SERVER["HTTPS"] != "on") {
// 	header("HTTP/1.1 301 Moved Permanently");
// 	header("Location: https://" . $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"]);
// 	exit();
// }
?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Canvas CSV Grade Report Generator</title>
  <link rel="stylesheet" href="_css/grade-csv.css">
</head>
<body>

<h1>Student Grade Report</h1>

<?php if (!isset($_POST["submitForm"])) { // no uploaded file?>

<p>Upload a Canvas gradebook CSV file to generate a printable report.</p>

<p>Files are uploaded over <strong>https</strong> (secure) and <strong>removed from server</strong> after the report is generated.</p>

<form action="generator" method="post" enctype="multipart/form-data">
	<label for="csv_file">Upload CSV file:</label>
	<input type="file" id="csv_file" name="csv_file" class="required" required/><br /><br />
	<label for="ignoreCategoryTotals">Omit Assignment Group totals: </label>
	<input type="checkbox" value="ignoreCategoryTotals" name="ignoreCategoryTotals" id="ignoreCategoryTotals" /><br /><br />
	<label for="gradedOnly">Calculate based only on graded assignments</label>
	<input type="checkbox" value="gradedOnly" name="gradedOnly" id="gradedOnly" /><br /><br />
	<input type="submit" name="submitForm" value="Generate Report" />
</form>

<p class="credit"><em>legal, disclaimers, etc.:<br />
-- this tool copyright &copy; <?php echo date("Y"); ?> <a href="https://mattlibera.dev">matt libera</a>, and offered as-is.<br />
-- i'll support if/as i can: <a href="mailto:matt.libera@uncg.edu">matt.libera@uncg.edu</a>.<br />
-- this tool is verified to work with the canvas gradebook csv export files as of <strong>december 2020</strong>. future canvas updates may someday break it, but i'll try my best to stay on top of it.<br />
-- user assumes all risk in use of this tool (please use on a secured network).<br />
-- again, no data is stored on the server, all transmissions are completed securely.</em></p>
<?php } else { // uploaded file was detected?>

<?php
// broad, shallow check for proper CSV format; reject all others.
// $csv_mimetypes = array(
// 	'text/csv',
// 	'text/plain',
// 	'application/csv',
// 	'text/comma-separated-values',
// 	'application/excel',
// 	'application/vnd.ms-excel',
// 	'application/vnd.msexcel',
// 	'text/anytext',
// 	'application/octet-stream',
// 	'application/txt',
// );

// if (!in_array($_FILES['csv_file']['type'], $csv_mimetypes)) {
// 	die("<p>Error: you need to submit a valid CSV file.</p>");
// }

$info = pathinfo($_FILES['csv_file']['name']);
// die(print_r($info));

if ($info['extension'] != 'csv') {
    die("<p>Error: you need to submit a valid CSV file.</p>");
}

$file = array_map('str_getcsv', file($_FILES['csv_file']['tmp_name'])); // one-liner magic to make array

// extra styles if ignoring assignment group totals
if (isset($_POST['ignoreCategoryTotals']) && $_POST["ignoreCategoryTotals"] != false) {
    ?>
	<style type="text/css">
	.ag {
		display: none;
	}
	</style>
	<?php
}

$lineCount = 0; // so that we only check for "Student" once.
foreach ($file as $index => $line) {
    if ($lineCount == 0) {
        if ($line[0] != "Student") {
            // not a valid CSV file
            die("<p>Error: not a valid Canvas Gradebook download CSV file.</p>");
        }
        // this is the header row of assignment names, etc.
        // starts with index 5 as the important stuff
        $assignmentNames = array_slice($line, 5);
    } elseif ($lineCount == 1) {
        // this is the points possible row
        // starts with index 5 as the important stuff
        $pointsPossible = array_slice($line, 5);
    } else {
        // on to a single student's information
        // index 0 is student's name
        // index 4 is section
        // index 5+ is student grades, corresponding to other arrays
        $studentName = $line[0];

        if ($studentName != "Test Student") { // we don't need to see his identification. er, grades.
            $studentSection = $line[4];
            $studentGrades = array_slice($line, 5);

            // output headers and table for current student?>
			<h2><?php echo $studentName; ?></h2>
			<h4><?php echo $studentSection; ?></h4>

			<table>
				<tr>
					<th>Assignment Name</th>
					<th>Score</th>
					<th>Out of</th>
				</tr>
			<?php for ($x = 0; $x < count($assignmentNames); $x++) {
                $aName = $assignmentNames[$x];
                $score = $studentGrades[$x];
                $poss = $pointsPossible[$x];

                $gradedOnly = isset($_POST['gradedOnly']) && $_POST['gradedOnly'];

                if ($gradedOnly && ($aName == "Final Score" || strpos($aName, "Final Score") !== false)) {
                    continue;
                }

                if ($aName === "Current Score") {
                    $class = $gradedOnly ? " class=\"final final-score\"" : " class=\"current ag\"";
                    $pct = "%";
                    $poss = "";
                } elseif (strpos($aName, "Current Score") !== false) {
                    $class = $gradedOnly ? " class=\"final ag\"" : " class=\"current ag\"";
                    $pct = "%";
                    $poss = "";
                } elseif ($aName == "Final Score") {
                    $class = " class=\"final final-score\"";
                    $pct = "%";
                    $poss = "";
                } elseif (strpos($aName, "Final Score") !== false) {
                    $class = " class=\"final ag\"";
                    $pct = "%";
                    $poss = "";
                } else {
                    $class = "";
                    $pct = "";
                } ?>
				<tr<?php echo $class; ?>>
					<td><?php echo $aName; ?></td>
					<td><?php echo $score; ?><?php echo $pct; ?></td>
					<td><?php echo $poss; ?></td>
				</tr>
			<?php
            } ?>
			</table>
			<?php
        }
    }

    $lineCount++; // increment here so that we can make sure to handle the different lines of the CSV file appropriately.
}
?>

<?php } // end master IF?>

	<script src="_js/jquery-1.10.2.min.js"></script>
	<script src="_js/jquery.validate.min.js"></script>
	<script>
	  // google analytics for domain
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-69070401-1', 'auto');
	  ga('send', 'pageview');

	</script>
</body>
</html>
